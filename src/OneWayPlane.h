//
// Created by sapphie on 26.05.19.
//

#ifndef RAYTRACING_ONEWAYPLANE_H
#define RAYTRACING_ONEWAYPLANE_H


#include "Plane.h"

class OneWayPlane : public Plane {
public:
  bool intersect(const Ray &_ray,
                 vec3 &_intersection_point,
                 vec3 &_intersection_normal,
                 double &_intersection_t) const override;
  explicit OneWayPlane(std::istream& is) : Plane(is){

  }
};


#endif //RAYTRACING_ONEWAYPLANE_H
