//=============================================================================
//
//   Exercise code for the lecture
//   "Introduction to Computer Graphics"
//   by Prof. Dr. Mario Botsch, Bielefeld University
//
//   Copyright (C) Computer Graphics Group, Bielefeld University.
//
//=============================================================================


//== INCLUDES =================================================================

#include "Plane.h"
#include <limits>
#include <random>


//== CLASS DEFINITION =========================================================



Plane::Plane(const vec3 &_center, const vec3 &_normal) : center(_center), normal(_normal) {
}


//-----------------------------------------------------------------------------

template <typename T>
inline double sgn(T arg) {
  if(arg<0) return -1;
  else if(arg>0) return 1;
  else return 0;
}

bool Plane::intersect(const Ray &_ray,
                      vec3 &_intersection_point,
                      vec3 &_intersection_normal,
                      double &_intersection_t) const {
  /** \todo
   * - compute the intersection of the plane with `_ray`
   * - if ray and plane are parallel there is no intersection
   * - otherwise compute intersection data and store it in `_intersection_point`, `_intersection_normal`, and `_intersection_t`.
   * - return whether there is an intersection in front of the viewer (t > 0)
  */
  double nd = dot(_ray.direction, normal);
  if (nd == 0) {
    _intersection_normal = vec3{0,0,0};
    return false;
  }

  double no = dot(normal, _ray.origin);
  double nc = dot(normal, center);
  // Return t
  _intersection_t = (nc-no)/nd;
  // Return intersection point
  _intersection_point = _ray.origin + _intersection_t * _ray.direction;

  // Return intersection normal
  double ndotn = dot(_ray.direction, normal);
  if(ndotn < 0) {
    _intersection_normal = normal;
  } else {
    _intersection_normal = -normal;
  }
  assert(dot(_intersection_normal, _ray.direction) <= 0);


  // Finally, return true iff t>0
  return _intersection_t > 0;
}

vec3 Plane::randomPointOnObject(std::mt19937_64 &g) const {
  std::normal_distribution<double> d(0,1000);
  // generate a random vector, then project it onto the plane
  vec3 res = {d(g),d(g),d(g)};
  vec3 projectionOnNormal = normal*(dot(normal, res));
  return res-projectionOnNormal;
}


//=============================================================================
