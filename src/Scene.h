//=============================================================================
//
//   Exercise code for the lecture
//   "Introduction to Computer Graphics"
//   by Prof. Dr. Mario Botsch, Bielefeld University
//
//   Copyright (C) Computer Graphics Group, Bielefeld University.
//
//=============================================================================

#ifndef SCENE_H
#define SCENE_H

//== INCLUDES =================================================================

#include "StopWatch.h"
#include "Object.h"
#include "Ray.h"
#include "Material.h"
#include "Image.h"
#include "Camera.h"
#include "ImageWindow.h"

#include <memory>
#include <string>
#include <random>
#include <optional>

//== CLASS DEFINITION =========================================================

/// \class Sphere Sphere.h
/// This class loads and raytraces scenes consisting of cameras, lights, and
/// objects
class Scene {
public:
  /// Constructor loads scene from file.
  explicit Scene(const std::string &path) {
    read(path);
    // for reproducible results
    generator = std::mt19937_64(1337); // NOLINT(cert-msc32-c)
  }


  enum class VisualisationType {
    Noise, MBudgetCinebench, NoAnim
  };



  struct VisualisationParam {
    VisualisationType kind;
    bool split_view;
    union {
      sf::Vector2u quadrantSize;
      uint noiseStep;
    };
  };

  std::uniform_real_distribution<> interval{0, 1};

  /// Allocate image and raytrace the scene.
  Image
  render(uint samplesPerPixel, bool NEE, uint numThreads, std::optional<ImageWindow> &imageWindow,
         VisualisationParam visualisationParam, bool autoclose);

  /// Determine the color seen by a viewing ray
  /**
  *   @param[in] _ray passed Ray
  *   @param[in] _depth holds the information, how many times the `_ray` had been reflected. Goes from 0 to max_depth. Should be used for recursive function call.
  *   @return    color
  **/
  vec3 trace(const Ray &_ray, int _depth, bool NEE, bool directContribution, double inMaterialRefractiveIndex = 1);

  /// Computes the closest intersection point between a ray and all objects in the scene.
  /**
  *       @param _ray Ray that should be tested for intersections with all objects in the scene.
  *       @param _Object_ptr Output parameter which holds the object from the scene, intersected by the `_ray`, closest to the `_ray`'s origin.
  *       @param _point returns intersection point
  *       @param _normal returns normal at `_point`
  *       @param _t returns distance between the `_ray`'s origin and `_point`
  *       @return returns `true`, if there is an intersection point between `_ray` and at least one object in the scene.
  **/
  bool intersect(const Ray &_ray, Object_ptr &, vec3 &_point, vec3 &_normal, double &_t);

  void read(const std::string &filename);

  size_t numObjects() const { return objects.size(); }

  // Accessors for scene objects and camera for debugging.
  const std::vector<std::shared_ptr<Object>> &getObjects() const { return objects; }

  const Camera &getCamera() const { return camera; }

  template<class T>
  void addObject(T &&o);

private:
  std::mt19937_64 generator;
  /// camera stores eye position, view direction, and can generate primary rays
  Camera camera;

  /// array for all lights in the scene
  std::vector<std::shared_ptr<Object>> lights;

  /// array for all the objects in the scene
  std::vector<std::shared_ptr<Object>> objects;

  /// max recursion depth for mirroring
  int max_depth = 0;

  /// background color
  vec3 background = vec3(0, 0, 0);

  /// global ambient light
  vec3 ambience = vec3(0, 0, 0);

  vec3 randomUniformVectorInHemisphere(const vec3 &n);

};

//=============================================================================
#endif // SCENE_H defined
//=============================================================================
