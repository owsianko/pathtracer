//
// Created by sapphie on 24.05.19.
//

#include "ImageWindow.h"
#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/System/Sleep.hpp>
#include <thread>
#include <iostream>
#include <assert.h>

void ImageWindow::run() {
  std::thread thr(&ImageWindow::runWindow, this);
  execution = std::move(thr);
  setActive(false);
}

void ImageWindow::runWindow() {
  while (isOpen()) {

    sf::Event e;
    while (pollEvent(e)) {
      switch (e.type) {
        case sf::Event::Closed:
          close();
          break;
        case sf::Event::KeyReleased:
          if (e.key.code == sf::Keyboard::Escape) {
            close();
          }
          break;
        default:
          break;
      }
    }

    display();
    clear(sf::Color::White);
    sf::Sprite sprite;
    sprite.setTexture(texture);
    draw(sprite);
    if(drawText) {
      draw(text);
    }
    sf::sleep(sf::milliseconds(33));
  }
  std::cout << "closing window" << std::endl;
}

ImageWindow::ImageWindow() : sf::RenderWindow(sf::VideoMode(50, 50), "Pathtracer", sf::Style::Default) {
  image.create(500, 500);
  sf::RenderWindow::setSize(image.getSize());
  texture.loadFromImage(image);
  qHeight = 500;
  qWidth = 500;
  font.loadFromFile("../fonts/UbuntuMono-R.ttf");
  text.setFont(font);
  text.setCharacterSize(32);
  text.setString("Samples per pixel: ");
  text.setOutlineColor(sf::Color::White);
  text.setOutlineThickness(1);
  text.setFillColor(sf::Color::Black);
}

ImageWindow::~ImageWindow() {
  execution.join();
}

void ImageWindow::setSize(uint width, uint height) {
  image.create(width, height);
  sf::RenderWindow::setSize({width, height});
  texture.loadFromImage(image);
  sf::Vector2f fSize = sf::Vector2f(texture.getSize());
  sf::FloatRect fView = sf::FloatRect(sf::Vector2f(0,0), fSize);
  sf::View view(fView);
  text.setCharacterSize(width/16);
  setView(view);

}

void ImageWindow::setQuadrantSize(uint quadWidth, uint quadHeight) {
  assert(image.getSize().x % quadWidth == 0);
  assert(image.getSize().y % quadHeight == 0);

  qWidth = quadWidth;
  qHeight = quadHeight;
}

void ImageWindow::setQuadrant(uint x, uint y, const std::vector<vec3> &quadrant) {
  assert(quadrant.size() == qHeight * qWidth);
  assert(x < image.getSize().x / qWidth);
  assert(y < image.getSize().y / qHeight);
  size_t xOffset = x * qWidth;
  size_t yOffset = y * qHeight;
  for (uint i = 0; i < qHeight; i++) {
    for (uint j = 0; j < qWidth; j++) {
      uint idx = qWidth*i+j;
      sf::Color col;
      col.r = static_cast<unsigned char>(quadrant[idx][0]*255);
      col.g = static_cast<unsigned char>(quadrant[idx][1]*255);
      col.b = static_cast<unsigned char>(quadrant[idx][2]*255);
      col.a = 255;
      image.setPixel(j+xOffset, image.getSize().y-(i+yOffset+1), col);
    }
  }
  texture.loadFromImage(image);
}

void ImageWindow::setDrawText(bool b) {
  drawText=b;
}

void ImageWindow::setNumSamples(uint numSamples) {
  text.setString("Samples per pixel: "+std::to_string(numSamples));
}

