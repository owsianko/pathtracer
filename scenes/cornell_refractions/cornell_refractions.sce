# camera: eye, center, up, fovy, width, height, lens radius, focal length
camera 0 10 -31  0 10 10  0 1 0  50  1024 1024 10 31

# recursion depth
depth 6

# background color
background 0 0 0

# planes: center, normal material

# floor
plane  0 0 0     0 1 0     1.0 1.0 1.0  0.0 0.0 0.0  0.0 0.0 0.0      0.0  0.0  0.0 0.0

# left
plane  10 0 0   -1 0 0     0.9 0.1 0.1  0.0 0.0 0.0  0.0 0.0 0.0      0.0  0.0  0.0 0.0

# right
plane  -10 0 0   1 0 0     0.1 0.1 0.9  0.0 0.0 0.0  0.0 0.0 0.0      0.0  0.0  0.0 0.0

# top
plane  0 20 0    0 -1 0    1.0 1.0 1.0  0.0 0.0 0.0  0.1 0.1 0.1      0.0  0.0  0.0 0.0

# back
plane  0 0 10    0 0 -1    1.0 1.0 1.0  0.0 0.0 0.0  0.0 0.0 0.0      0.0  0.0  0.0 0.0

# front, behind the camera
one-way-plane 0 0 -10  0 0 1     0   0   0    0   0   0    0   0   0        0    0  0.0 0.0

# front, for debug
#plane  0 0 -10     0 0  1    1.0 1.0 1.0  0.0 0.0 0.0  1.0 1.0 1.0      0.0  0.0

# meshes: filename, shading, material (ambient, diffuse, specular, shininess)
mesh light.off   FLAT 0.0 0.0 0.0   0.0 0.0 0.0   50.0 50.0 50.0   0.0 0.0   0   0
#mesh diamond.off FLAT 1.0 1.0 1.0   0.0 0.0 0.0   50.0 50.0 50.0   0.0 0.0   0.7 2.41

# spheres: center, radius, material
sphere    6.0 2.0 0   2   0.25 0.25 0.25  0.0 0.0 0.0  0.0 0.0 0.0  0 0.4  0   0
sphere    0.0 3.0 0   3   1.0  1.0  1.0   0.0 0.0 0.0  0.0 0.0 0.0  0 0    0.7 2.41
sphere   -6.0 2.0 0   2   0.25 0.25 0.25  0.0 0.0 0.0  0.0 0.0 0.0  0 0.4  0   0


# tmp light
# spheres: center, radius, material
#sphere   0.0 2.0 0   2   0.0 0.0 0.0  0.0 0.0 0.0  9.0 9.0 9.0  0 0  0.0 0.0
