//=============================================================================
//
//   Exercise code for the lecture
//   "Introduction to Computer Graphics"
//   by Prof. Dr. Mario Botsch, Bielefeld University
//
//   Copyright (C) Computer Graphics Group, Bielefeld University.
//
//=============================================================================

//== INCLUDES =================================================================
#include "Scene.h"

#include "Plane.h"
#include "Sphere.h"
#include "Cylinder.h"
#include "Mesh.h"
#include "MirroredSphere.h"
#include "OneWayPlane.h"

#include <limits>
#include <map>
#include <functional>
#include <stdexcept>
#include <omp.h>

#if HAS_TBB
#include <tbb/tbb.h>
#include <tbb/parallel_for.h>
#endif

uint findClosestFactor(unsigned int toFactor, uint goal);

//-----------------------------------------------------------------------------
vec3 Scene::randomUniformVectorInHemisphere(const vec3 &n) {

  // cosTheta = 1-y
  double cosTheta = interval(generator);
  double sinTheta = sqrt(1 - cosTheta * cosTheta);
  double phi = interval(generator) * M_PI * 2;
  auto basis = (getBasis(n));
  double y = sinTheta * cos(phi);
  double z = -sinTheta * sin(phi);
  double x = cosTheta;
  vec3 res = x * basis[0] + y * basis[1] + z * basis[2];
  return res;
}

inline const vec3 nudge(const vec3 &p, const vec3 &dir);

Image Scene::render(uint samplesPerPixel, bool NEE, uint numThreads, std::optional<ImageWindow> &imageWindow,
                    VisualisationParam visualisationParam, bool autoclose) {
  assert(samplesPerPixel > 0);
  // allocate new image.
  Image img(camera.width, camera.height);


  uint quadrantHeight = 1;
  uint quadrantWidth = 1;
  // set up window
  if (imageWindow) {
    imageWindow->setSize(camera.width, camera.height);

    // set quadrant size
    if (visualisationParam.kind == VisualisationType::MBudgetCinebench) {

      quadrantWidth = visualisationParam.quadrantSize.x;
      quadrantHeight = visualisationParam.quadrantSize.y;

      // check if quadrand sizes are ok
      if (camera.width % quadrantWidth != 0) {
        quadrantWidth = findClosestFactor(camera.width, quadrantWidth);
      }

      if (camera.height % quadrantHeight != 0) {
        quadrantHeight = findClosestFactor(camera.height, quadrantHeight);
      }
      imageWindow->setQuadrantSize(quadrantWidth, quadrantHeight);
    } else {
      // in that case we'll be drawing to the entire window
      imageWindow->setQuadrantSize(camera.width, camera.height);
    }
  }

  bool split_view = visualisationParam.split_view;
  auto raytraceQuadrant = [&img, this, samplesPerPixel, NEE, quadrantHeight, quadrantWidth, &imageWindow, split_view](
          uint xQuad,
          uint yQuad) {
    uint xOffset = xQuad * quadrantWidth;
    uint yOffset = yQuad * quadrantHeight;
    std::vector<vec3> quadrant(quadrantHeight * quadrantWidth, vec3{0, 0, 0});
    for (uint i = 0; i < quadrantHeight; i++) {
      for (uint j = 0; j < quadrantWidth; j++) {

        bool actualNEE = NEE;
        uint x = xOffset + j;
        if (x > camera.width / 2) actualNEE = NEE ^ split_view;
        uint y = yOffset + i;
        uint idx = j + i * quadrantWidth;

        vec3 color(0, 0, 0);
        for (uint sample = 0; sample < samplesPerPixel; sample++) {
          Ray ray = camera.ray_on_lens(x, y, generator);
          color += trace(ray, 0, actualNEE, true);
        }

        color /= float(samplesPerPixel);
        color = min(color, vec3(1, 1, 1));
        img(x, y) = color;
        quadrant[idx] = color;
      }

      // Defensive programming; normally this shouldn't ever execute if there isn't a window
      if (imageWindow) {
        imageWindow->setQuadrant(xQuad, yQuad, quadrant);
      }
    }
  };

  // Function rendering a full row of the image
  auto raytraceRow = [&img, this, &samplesPerPixel, NEE, split_view](uint y) {
    for (uint x = 0; x < camera.width; ++x) {
      vec3 color = vec3{0.0};
      bool actualNEE = NEE;
      if (x > camera.width / 2) {
        actualNEE = NEE ^ split_view;
      }
      for (uint sample = 0; sample < samplesPerPixel; sample++) {
        Ray ray = camera.ray_on_lens(x, y, generator);

        // compute color by tracing this ray
        color += trace(ray, 0, actualNEE, true);

      }
      color /= float(samplesPerPixel);
      // avoid over-saturation
      color = min(color, vec3(1, 1, 1));

      // store pixel color
      img(x, y) = color;
    }
  };

  uint numQuadsX = camera.width / quadrantWidth;
  uint numQuadsY = camera.height / quadrantHeight;
  // If possible, raytrace image columns in parallel. We use TBB if available
  // and try OpenMP otherwise. Note that OpenMP only works on the latest
  // clang compilers, so macOS users will probably have the best luck with TBB.
  // You can install TBB with MacPorts/Homebrew, or from Intel:
  // https://github.com/01org/tbb/releases
  switch (visualisationParam.kind) {
    case VisualisationType::MBudgetCinebench: {

      /// Cinebench-like tile render
#if defined(_OPENMP)
#pragma omp parallel for schedule(dynamic) num_threads(numThreads) collapse(2)
#endif
      for (uint x = 0; x < numQuadsX; x++) {
        for (uint y = 0; y < numQuadsY; y++) {
          raytraceQuadrant(x, y);
        }
      }
      break;
    }

      /// No visualisation
    case VisualisationType::NoAnim: {
      std::vector<vec3> loadingBar;
      const uint loadingBarWidth = 5;
      if (imageWindow) {
        imageWindow->setQuadrantSize(loadingBarWidth, camera.height);
        for(uint i = 0; i<camera.height*loadingBarWidth; i++) {
          loadingBar.emplace_back(0,0,0);
        }
      }
#if defined(_OPENMP)
#pragma omp parallel for schedule(dynamic) num_threads(numThreads)
#endif
      for (uint y = 0; y < camera.height; y++) {
        raytraceRow(y);
        if (imageWindow) {
          for(uint x = 0; x<loadingBarWidth; x++) {
            loadingBar[x+y*loadingBarWidth] = {1,1,1};
          }
          imageWindow->setQuadrant(0,0, loadingBar);
        }
      }

      if (imageWindow) {
        std::vector<vec3> image;
        image.reserve(camera.height * camera.width);
        for (uint y = 0; y < camera.height; y++) {
          for (uint x = 0; x < camera.width; x++) {
            image.push_back(img(x, y));
          }
        }
        imageWindow->setQuadrantSize(camera.width, camera.height);
        imageWindow->setQuadrant(0, 0, image);
      }
      break;
    }

    case VisualisationType::Noise:

      if (imageWindow) {
        imageWindow->setDrawText(true);
      }
      std::vector<vec3> intermediaryResult(camera.width * camera.height, vec3{0, 0, 0});

      for (uint sample = 1; sample <= samplesPerPixel; sample++) {
#if defined(_OPENMP)
#pragma omp parallel for schedule(dynamic) num_threads(numThreads) collapse(2)
#endif
        for (uint y = 0; y < camera.height; y++) {
          for (uint x = 0; x < camera.width; x++) {
            bool actualNee = NEE;
            if (x > camera.width / 2) {
              actualNee = NEE ^ split_view;
            }
            Ray ray = camera.ray_on_lens(x, y, generator);
            img(x, y) += trace(ray, 0, actualNee, true);
            if (sample % visualisationParam.noiseStep == 0 || sample == 1) {
              uint idx = x + camera.width * y;
              intermediaryResult[idx] = img(x, y) / float(sample);
              intermediaryResult[idx] = min(intermediaryResult[idx], vec3{1, 1, 1});
            }
          }
        }

        if (imageWindow && (sample % visualisationParam.noiseStep == 0 || sample == 1)) {
          imageWindow->setQuadrant(0, 0, intermediaryResult);
          imageWindow->setNumSamples(sample);
        }


      }

      // Copy result
      for (uint x = 0; x < camera.width; x++) {

        for (uint y = 0; y < camera.height; y++) {
          img(x, y) = img(x, y) / samplesPerPixel;
          img(x, y) = min(img(x, y), vec3{1, 1, 1});
        }

      }
      break;
  }

  if (autoclose) {
    if (imageWindow) {
      imageWindow->close();
    }
  }
  // Note: compiler will elide copy.
  return img;
}

//-----------------------------------------------------------------------------
vec3 Scene::trace(const Ray &_ray, int _depth, bool NEE, bool directContribution, double inMaterialRefractiveIndex) {
  // stop if recursion depth (=number of reflections) is too large
  if (_depth > max_depth) return vec3(0, 0, 0);

  // Find first intersection with an object. If an intersection is found,
  // it is stored in object, point, normal, and t.
  Object_ptr object;
  vec3 point;
  vec3 normal;
  double t;
  if (!intersect(_ray, object, point, normal, t)) {
    return background;
  }

  vec3 color = {0, 0, 0};

  vec3 randomDir = randomUniformVectorInHemisphere(normal);
  assert(norm(randomDir) <= 1.01 && norm(randomDir) >= 0.99);
  assert(dot(randomDir, normal) >= 0);
  vec3 randomRayOrigin = nudge(point, randomDir);
  Ray newRay = Ray(randomRayOrigin, randomDir);

  double cosTheta = dot(randomDir, normal);
  vec3 brdf = object->material.diffuse / M_PI;
  double uniformPdf = M_1_PI / 2; // 1/2pi
  //*
  vec3 directLight = {0, 0, 0};
  if (NEE) {
    for (const auto &light: lights) {

      // For some cases, arithmetic errors can lead to the ray missing the light
      const uint maxTries = 20;
      uint tries = 0;
      bool intersected = false;
      do {

        vec3 randomPtOnLight = light->randomPointOnObject(generator);
        vec3 toLight = randomPtOnLight - point;
        vec3 shadowRayOrigin = nudge(point, toLight);
        double distanceToLight = norm(toLight);
        vec3 explicitShadowDirection = normalize(toLight);
        Ray shadowRay = Ray(shadowRayOrigin, explicitShadowDirection);
        Object_ptr hit;
        vec3 pt, n;
        double distanceToImpact = 0;
        // Ray parallel to light source

        // shoot shadow ray
        if (intersect(shadowRay, hit, pt, n, distanceToImpact)) {
          // if we hit the right light source
          if (hit == light.get()) {
            // if we hit it on the right side
            if (distanceToImpact < distanceToLight * 1.01 && distanceToLight < distanceToImpact * 1.01) {
              double nl = dot(explicitShadowDirection, normal);
              if (nl > 0) {
                double jacobian = abs(dot(n, explicitShadowDirection)) / (distanceToImpact);
                vec3 lightFromSource = light->material.emittance * jacobian * nl;
                directLight += lightFromSource;
              }
            }
          }
          intersected = true;
        }
        tries++;
      } while (!intersected && maxTries > tries);
    }
  }

  auto hitColor = trace(newRay, _depth + 1, NEE, false);
  color += (hitColor / uniformPdf * cosTheta + directLight) * brdf;

  if (!NEE || directContribution) {
    color += object->material.emittance;
  }

  // don't shoot additional ray if it wouldn't be counted anyway
  const double reflectance = object->material.mirror;
  if (reflectance > 0) {
    const vec3 reflectedDir = reflect(_ray.direction, normal);
    const vec3 reflectedOrigin = nudge(point, reflectedDir);
    const Ray reflectionRay = Ray(reflectedOrigin, reflectedDir);

    const vec3 reflectedColor = trace(reflectionRay, _depth + 1, NEE, true);
    color = (reflectance) * reflectedColor + (1 - reflectance) * color;
  }

  const double transparency = object->material.transparency;
  if (transparency > 0) {
    const vec3 reflectedDirPar = normal * dot(normal, _ray.direction);
    const vec3 reflectedDirPerp = _ray.direction - reflectedDirPar;
    const vec3 inMaterialPerp = reflectedDirPerp * (inMaterialRefractiveIndex / object->material.refractive_index);
    const vec3 refractedRayDir = inMaterialPerp + reflectedDirPar;
    const vec3 refractedRayOrigin = nudge(point, refractedRayDir);
    const Ray refractionRay = Ray(refractedRayOrigin, refractedRayDir);

    const vec3 refractedColor = trace(refractionRay, _depth + 1, NEE, true);
    color = transparency * refractedColor + (1 - transparency) * color;
  }
  return color;
}

//-----------------------------------------------------------------------------

bool Scene::intersect(const Ray &_ray, Object_ptr &_object, vec3 &_point, vec3 &_normal, double &_t) {
  double t, tmin(Object::NO_INTERSECTION);
  vec3 p, n;

  for (const auto &o: objects) // for each object
  {
    if (o->intersect(_ray, p, n, t)) // does ray intersect object?
    {
      if (t < tmin) // is intersection point the currently closest one?
      {
        tmin = t;
        _object = o.get();
        _point = p;
        _normal = n;
        _t = t;
      }
    }
  }

  return (tmin != Object::NO_INTERSECTION);
}


//-----------------------------------------------------------------------------

void Scene::read(const std::string &_filename) {
  std::ifstream ifs(_filename);
  if (!ifs)
    throw std::runtime_error("Cannot open file " + _filename);

  const std::map<std::string, std::function<void(void)>> entityParser = {
          {"depth",         [&]() { ifs >> max_depth; }},
          {"camera",        [&]() { ifs >> camera; }},
          {"background",    [&]() { ifs >> background; }},
          {"ambience",      [&]() { ifs >> ambience; }},
          {"plane",         [&]() { addObject(Plane(ifs)); }},
          {"sphere",        [&]() { addObject(Sphere(ifs)); }},
          {"cylinder",      [&]() { addObject(Cylinder(ifs)); }},
          {"mesh",          [&]() { addObject(Mesh(ifs, _filename)); }},
          {"one-way-plane", [&]() { addObject(OneWayPlane(ifs)); }},
//          {"mirrored-sphere",[&]() { addObject(MirroredSphere(ifs));}}
  };

  // parse file
  std::string token;
  while (ifs && (ifs >> token) && (!ifs.eof())) {
    if (token[0] == '#') {
      ifs.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
      continue;
    }

    if (entityParser.count(token) == 0)
      throw std::runtime_error("Invalid token encountered: " + token);
    entityParser.at(token)();
  }
}

template<class T>
void Scene::addObject(T &&o) {
  auto obj = std::make_shared<T>(o);
  objects.emplace_back(obj);
  if (obj->isLight()) {
    lights.emplace_back(obj);
  }
}

// we're dealing with small enough numbers here that we can do this without feeling ashamed
uint findClosestFactor(unsigned int toFactor, uint goal) {
  std::vector<uint> factors;
  for (uint i = 1; i * i < toFactor; i++) {
    if (toFactor % i == 0) {
      factors.push_back(i);
      factors.push_back(toFactor / i);
    }
  }

  uint closest = 1;
  for (uint i : factors) {
    uint distance = i < goal ? goal - i : i - goal;
    if (distance < closest) {
      closest = i;
    }
  }
  return closest;
}

inline const vec3 nudge(const vec3 &p, const vec3 &dir) {
  // estimated empirically
  double eta = 1e-6;
  return p + dir * eta;
}
//=============================================================================
