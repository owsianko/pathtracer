//=============================================================================
//                                                                            
//   Exercise code for the lecture
//   "Introduction to Computer Graphics"
//   by Prof. Dr. Mario Botsch, Bielefeld University
//
//   Copyright (C) Computer Graphics Group, Bielefeld University.
//                                                                            
//=============================================================================

//== INCLUDES =================================================================

#include "vec3.h"
std::array<vec3, 3> getBasis(const vec3& n) {
  vec3 x,y,z;
  if(n[0] == n[1] && n[1] == n[2] && n[0] == 0.0) {
    return {x,y,z};
  } else {
    x = normalize(n);
    // for numerical robustness
    if(abs(x[0]) < abs(x[1]) && abs(x[0]) < abs(x[2])) {
      y[0] = 0;
      y[1] = -x[2];
      y[2] = x[1];
    } else if (abs(x[1]) < abs(x[2])) { //x_1 is smallest
      y[2] = -x[0];
      y[1] = 0;
      y[0] = x[2];
    } else { // x_2 is smallest
      y[0] = -x[1];
      y[1] = x[0];
      y[2] = 0;
    }
    y = normalize(y);
    // no need to re-normalize
    z = cross(x,y);
#define ETA 1e-1
    assert(abs(norm(x)-1)<ETA);
    assert(abs(norm(y)-1)<ETA);
    assert(abs(norm(z)-1)<ETA);
#undef ETA
    return {x,y,z};
  }
}

mat3 transpose(const mat3 &m) {
  mat3 newMat;
  for(uint i = 0; i<3; i++) {
    for(uint j = 0; j<3; j++) {
      newMat[i][j] = m[j][i];
    }
  }
  return newMat;
};



//=============================================================================
