//=============================================================================
//                                                                            
//   Exercise code for the lecture
//   "Introduction to Computer Graphics"
//   by Prof. Dr. Mario Botsch, Bielefeld University
//
//   Copyright (C) Computer Graphics Group, Bielefeld University.
//                                                                            
//=============================================================================

#ifndef MATERIAL_H
#define MATERIAL_H


//== INCLUDES =================================================================

#include "vec3.h"


//== CLASS DEFINITION =========================================================


/// \class Material Material.h
/// This class stores all material parameters.
struct Material {

  /// diffuse color
  vec3 diffuse;

  /// specular color
  vec3 specular;

  /// light emittance
  vec3 emittance;

  /// shininess factor
  double shininess;

  /// reflectivity factor (1=perfect mirror, 0=no reflection).
  double mirror;

  double transparency;
  double refractive_index;
};


//-----------------------------------------------------------------------------


/// read material from stream
inline std::istream &operator>>(std::istream &is, Material &m) {
  is >>  m.diffuse >> m.specular >> m.emittance >> m.shininess >> m.mirror >> m.transparency >> m.refractive_index;
  return is;
}


//=============================================================================
#endif // MATERIAL_H defined
//=============================================================================
