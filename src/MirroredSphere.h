//
// Created by sapphie on 20.05.19.
//

#ifndef RAYTRACING_MIRROREDSPHERE_H
#define RAYTRACING_MIRROREDSPHERE_H


#include "Object.h"
#include "Sphere.h"

class MirroredSphere : public Object {
public:
  explicit MirroredSphere(std::istream &is) { parse(is); };
  bool intersect(const Ray &ray, vec3 &intersectionPoint, vec3 &normal, double& t) const override ;
  vec3 randomPointOnObject(std::mt19937_64 &g) const override;
private:
  Sphere underlying;
};


#endif //RAYTRACING_MIRROREDSPHERE_H
