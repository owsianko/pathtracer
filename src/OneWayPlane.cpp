//
// Created by sapphie on 26.05.19.
//

#include "OneWayPlane.h"

bool OneWayPlane::intersect(const Ray &_ray, vec3 &_intersection_point, vec3 &_intersection_normal,
                            double &_intersection_t) const {
  if (dot(_ray.direction, normal) < 0)
    return Plane::intersect(_ray, _intersection_point, _intersection_normal, _intersection_t);
  else
    return false;

}
