# camera: eye, center, up, fovy, width, height
camera 2 2 -10   0   0   0  0 1 0  45  512  512

# recursion depth
depth  2

# light spheres: center, radius, material
sphere  0  0   0      0.5     0.5 0.5 0.5  0.0 0.0 0.0  0   0   0   0     0
sphere  0  0   0      50      1.0 0.0 0.0  0.0 0.0 0.0  1.0 1.0 1.0 0     0

#1.0 1.0 1.0  1.0 1.0 1.0  1.0 1.0 1.0