//=============================================================================
//
//   Exercise code for the lecture
//   "Introduction to Computer Graphics"
//   by Prof. Dr. Mario Botsch, Bielefeld University
//
//   Copyright (C) Computer Graphics Group, Bielefeld University.
//
//=============================================================================

//== INCLUDES =================================================================

#include "Mesh.h"
#include "Plane.h"
#include <fstream>
#include <string>
#include <stdexcept>
#include <limits>
#include <array>


//== IMPLEMENTATION ===========================================================

void printTriangles(const std::vector<Mesh::Triangle> &triangles) {
  for (auto tr : triangles) {
    std::cout << tr.i0 << " "
              << tr.i1 << " "
              << tr.i2 << " "
              << std::endl;
  }
}


Mesh::Mesh(std::istream &is, const std::string &scenePath) {
  std::string meshFile, mode;
  is >> meshFile;

  // load mesh from file
  read(scenePath.substr(0, scenePath.find_last_of("/\\") + 1) + meshFile); // Use both Unix and Windows path separators

  is >> mode;
  if (mode == "FLAT") draw_mode_ = FLAT;
  else if (mode == "PHONG") draw_mode_ = PHONG;
  else throw std::runtime_error("Invalid draw mode " + mode);

  is >> material;
}


//-----------------------------------------------------------------------------


bool Mesh::read(const std::string &_filename) {
  // read a mesh in OFF format


  // open file
  std::ifstream ifs(_filename);
  if (!ifs) {
    std::cerr << "Can't open " << _filename << "\n";
    return false;
  }


  // read OFF header
  std::string s;
  unsigned int nV, nF, dummy, i;
  ifs >> s;
  if (s != "OFF") {
    std::cerr << "No OFF file\n";
    return false;
  }
  ifs >> nV >> nF >> dummy;
  std::cout << "\n  read " << _filename << ": " << nV << " vertices, " << nF << " triangles";


  // read vertices
  Vertex v;
  vertices_.clear();
  vertices_.reserve(nV);
  for (i = 0; i < nV; ++i) {
    ifs >> v.position;
    vertices_.push_back(v);
  }


  // read triangles
  Triangle t;
  triangles_.clear();
  triangles_.reserve(nF);
  for (i = 0; i < nF; ++i) {
    ifs >> dummy >> t.i0 >> t.i1 >> t.i2;
    triangles_.push_back(t);
  }


  // close file
  ifs.close();


  // compute face and vertex normals
  compute_normals();

  // compute the total area of the shape's triangles
  compute_area();

  // compute bounding box
  compute_bounding_box();


  return true;
}


//-----------------------------------------------------------------------------

// Determine the weights by which to scale triangle (p0, p1, p2)'s normal when
// accumulating the vertex normals for vertices 0, 1, and 2.
// (Recall, vertex normals are a weighted average of their incident triangles'
// normals, and in our raytracer we'll use the incident angles as weights.)
// \param[in] p0, p1, p2    triangle vertex positions
// \param[out] w0, w1, w2    weights to be used for vertices 0, 1, and 2
void angleWeights(const vec3 &p0, const vec3 &p1, const vec3 &p2,
                  double &w0, double &w1, double &w2) {
  // compute angle weights
  const vec3 e01 = normalize(p1 - p0);
  const vec3 e12 = normalize(p2 - p1);
  const vec3 e20 = normalize(p0 - p2);
  w0 = acos(std::max(-1.0, std::min(1.0, dot(e01, -e20))));
  w1 = acos(std::max(-1.0, std::min(1.0, dot(e12, -e01))));
  w2 = acos(std::max(-1.0, std::min(1.0, dot(e20, -e12))));
}


//-----------------------------------------------------------------------------

void Mesh::compute_normals() {
  // compute triangle normals
  for (Triangle &t: triangles_) {
    const vec3 &p0 = vertices_[t.i0].position;
    const vec3 &p1 = vertices_[t.i1].position;
    const vec3 &p2 = vertices_[t.i2].position;
    t.normal = normalize(cross(p1 - p0, p2 - p0));
  }

  // initialize vertex normals to zero
  for (Vertex &v: vertices_) {
    v.normal = vec3(0, 0, 0);
  }

  /** \todo
   * In some scenes (e.g the office scene) some objects should be flat
   * shaded (e.g. the desk) while other objects should be Phong shaded to appear
   * realistic (e.g. chairs). You have to implement the following:
   * - Compute vertex normals by averaging the normals of their incident triangles.
   * - Store the vertex normals in the Vertex::normal member variable.
   * - Weigh the normals by their triangles' angles.
   */
  for (const Triangle &t: triangles_) {
    // Grab vertices
    Vertex &v0 = vertices_[t.i0];
    Vertex &v1 = vertices_[t.i1];
    Vertex &v2 = vertices_[t.i2];

    double w0, w1, w2;
    angleWeights(v0.position, v1.position, v2.position, w0, w1, w2);

    v0.normal += w0 * t.normal;
    v1.normal += w1 * t.normal;
    v2.normal += w2 * t.normal;
  }

  for (Vertex &v: vertices_) {
    v.normal = normalize(v.normal);
  }

}


//-----------------------------------------------------------------------------


void Mesh::compute_bounding_box() {
  bb_min_ = vec3(std::numeric_limits<double>::max());
  bb_max_ = vec3(std::numeric_limits<double>::lowest());

  for (Vertex v: vertices_) {
    bb_min_ = min(bb_min_, v.position);
    bb_max_ = max(bb_max_, v.position);
  }
}


//-----------------------------------------------------------------------------

class Interval {
public:
  Interval() = delete;

  Interval(double _d1, double _d2);

  Interval intersect(const Interval &that);

  double size();

  inline static Interval rightOpenInterval(double min);

private:
  double min;
  double max;
};

Interval::Interval(double _d1, double _d2) {
  if (_d1 > _d2) {
    min = _d2;
    max = _d1;
  } else {
    min = _d1;
    max = _d2;
  }
}

Interval Interval::intersect(const Interval &that) {
  if (that.min > max || min > that.max) {
    return {0.0, 0.0};
  } else {
    // Pick the bigger minimum
    double newMin = (min > that.min) ? min : that.min;
    // And the smaller maximum
    double newMax = (max < that.max) ? max : that.max;
    return {newMin, newMax};
  }
}

Interval Interval::rightOpenInterval(double min) {
  return {min, INFINITY};
}

double Interval::size() {
  return max - min;
}

bool Mesh::intersect_bounding_box(const Ray &_ray) const {

#define MIN_TRIANGLES 10
  if (triangles_.size() < MIN_TRIANGLES) return true;
  /** \todo
  * Intersect the ray `_ray` with the axis-aligned bounding box of the mesh.
  * Note that the minimum and maximum point of the bounding box are stored
  * in the member variables `bb_min_` and `bb_max_`. Return whether the ray
  * intersects the bounding box.
  * This function is ued in `Mesh::intersect()` to avoid the intersection test
  * with all triangles of every mesh in the scene. The bounding boxes are computed
  * in `Mesh::compute_bounding_box()`.
  */
  double dx = _ray.direction[0];
  double dy = _ray.direction[1];
  double dz = _ray.direction[2];

  double ox = _ray.origin[0];
  double oy = _ray.origin[1];
  double oz = _ray.origin[2];

  double minx = bb_min_[0];
  double miny = bb_min_[1];
  double minz = bb_min_[2];

  double maxx = bb_max_[0];
  double maxy = bb_max_[1];
  double maxz = bb_max_[2];

  if (maxx - minx == 0 || maxy - miny == 0 || maxz - minz == 0) return true;

  Interval tx_interval = Interval::rightOpenInterval(0);
  Interval ty_interval = Interval::rightOpenInterval(0);
  Interval tz_interval = Interval::rightOpenInterval(0);

  // Ray doesn't move in x direction
  if (dx == 0.0) {
    // Ray is outside box, and won't ever get into it
    if (ox < minx || ox > maxx) {
      return false;
    }
    // Otherwise the ray is already in the box

    // Ray does move in x direction, shrink possibilies accordingly
  } else {
    double newMin = (minx - ox) / dx;
    double newMax = (maxx - ox) / dx;
    tx_interval = tx_interval.intersect({newMin, newMax});
  }

  // rinse and repeat
  if (dy == 0.0) {
    if (oy < miny || oy > maxy) {
      return false;
    }
  } else {
    double newMin = (miny - oy) / dy;
    double newMax = (maxy - oy) / dy;
    ty_interval = ty_interval.intersect({newMin, newMax});
  }

  // and repeat
  if (dz == 0.0) {
    if (oz < minz || oz > maxz) {
      return false;
    }
  } else {
    double newMin = (minz - oz) / dz;
    double newMax = (maxz - oz) / dz;
    tz_interval = tz_interval.intersect({newMin, newMax});
  }

  return tx_interval.intersect(ty_interval.intersect(tz_interval)).size() > 0;
}


//-----------------------------------------------------------------------------


bool Mesh::intersect(const Ray &_ray,
                     vec3 &_intersection_point,
                     vec3 &_intersection_normal,
                     double &_intersection_t) const {
  // check bounding box intersection
  if (!intersect_bounding_box(_ray)) {
    return false;
  }

  vec3 p, n;
  double t;

  _intersection_t = NO_INTERSECTION;

  // for each triangle
  for (const Triangle &triangle : triangles_) {
    // does ray intersect triangle?
    if (intersect_triangle(triangle, _ray, p, n, t)) {
      // is intersection closer than previous intersections?
      if (t < _intersection_t) {
        // store data of this intersection
        _intersection_t = t;
        _intersection_point = p;
        _intersection_normal = n;
      }
    }
  }

  return (_intersection_t != NO_INTERSECTION);
}


//-----------------------------------------------------------------------------

inline double det2(const vec3 &m0, const vec3 &m1, unsigned row) {
  if (row == 0) {
    return m0[1] * m1[2] - m0[2] * m1[1];
  } else if (row == 1) {
    return m0[0] * m1[2] - m0[2] * m1[0];
  } else if (row == 2) {
    return m0[0] * m1[1] - m0[1] * m1[0];
  } else {
    assert (false);
    return 0;
  }
}

inline double det(const vec3 &m0, const vec3 &m1, const vec3 &m2) {
  return m0[0] * det2(m1, m2, 0) - m0[1] * det2(m1, m2, 1) + m0[2] * det2(m1, m2, 2);
}

bool Mesh::intersect_triangle(const Triangle &_triangle,
                              const Ray &_ray,
                              vec3 &_intersection_point,
                              vec3 &_intersection_normal,
                              double &_intersection_t) const {
  const Vertex &v0 = vertices_[_triangle.i0];
  const Vertex &v1 = vertices_[_triangle.i1];
  const Vertex &v2 = vertices_[_triangle.i2];

  const vec3 &p0 = v0.position;
  const vec3 &p1 = v1.position;
  const vec3 &p2 = v2.position;

  const vec3 &d = -_ray.direction;
  const vec3 result_column = _ray.origin - p0;
  const vec3 col2 = p1 - p0;
  const vec3 col3 = p2 - p0;

  _intersection_normal = _triangle.normal;
  const double determinant = det(d, col2, col3);
  if (determinant != 0) {

    _intersection_t = det(result_column, col2, col3) / determinant;
    // Check if intersection happens in front of viewer
    if (_intersection_t > 0) {
      double beta = det(d, result_column, col3) / determinant;
      if (beta >= 0) {
        double gamma = det(d, col2, result_column) / determinant;
        double alpha = 1 - beta - gamma;

        // Check if intersection with triangle actually happens
        if (gamma >= 0 && alpha >= 0) {
          _intersection_point = _ray(_intersection_t);

          // Compute normal
          if (draw_mode_ == FLAT) {
            _intersection_normal = _triangle.normal;
          } else {
            _intersection_normal = alpha * v0.normal + beta * v1.normal + gamma * v2.normal;
            _intersection_normal = normalize(_intersection_normal);
          }
          if (dot(_intersection_normal, _ray.direction) > 0) {
            _intersection_normal = -_intersection_normal;
          }

          return true;
        }
      }
    }
  }
  return false;
}

vec3 Mesh::randomPointOnObject(std::mt19937_64 &g) const {
  std::uniform_real_distribution<double> uniDist(0, 1);
  // pick a random triangle

  double number = uniDist(g) * totalArea;
  size_t idx;
  for (idx = 0; idx < triangles_.size() && areas[idx] <= number; number -= areas[idx], idx++);
  assert(idx < triangles_.size());
  const Triangle &triangle = triangles_[idx];

  // pick a point inside the triangle
  double alpha = uniDist(g);
  double beta = uniDist(g);
  if (alpha + beta > 1) {
    alpha = 1 - alpha;
    beta = 1 - beta;
  }
  double gamma = 1 - alpha - beta;
  assert(abs(gamma + beta + alpha - 1) < 1e-5);
  assert(alpha >= 0);
  assert(beta >= 0);
  assert(gamma >= 0);
  std::array<vec3, 3> positions;
  positions[0] = vertices_[triangle.i0].position;
  positions[1] = vertices_[triangle.i1].position;
  positions[2] = vertices_[triangle.i2].position;
  vec3 res = alpha * positions[1] + beta * positions[2] + gamma * positions[0];
  return res;
}

double Mesh::area(const Mesh::Triangle &t) const {
  std::array<vec3, 3> positions;
  positions[0] = vertices_[t.i0].position;
  positions[1] = vertices_[t.i1].position;
  positions[2] = vertices_[t.i2].position;

  // Thanks wolfram
  vec3 a = positions[2] - positions[0];
  vec3 b = positions[2] - positions[1];
  vec3 aCrossB = cross(a, b);
  return 0.5 * norm(aCrossB);
}

void Mesh::compute_area() {
  totalArea = 0;
  for (auto const &triangle: triangles_) {
    double currArea = area(triangle);
    areas.push_back(currArea);
    assert(currArea > 0);
    totalArea += currArea;
  }
  assert(areas.size() == triangles_.size());
}


//=============================================================================
