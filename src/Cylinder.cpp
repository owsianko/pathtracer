//=============================================================================
//
//   Exercise code for the lecture
//   "Introduction to Computer Graphics"
//   by Prof. Dr. Mario Botsch, Bielefeld University
//
//   Copyright (C) Computer Graphics Group, Bielefeld University.
//
//=============================================================================

//== INCLUDES =================================================================

#include "Cylinder.h"
#include "SolveQuadratic.h"

#include <array>
#include <cmath>

//== IMPLEMENTATION =========================================================

template <typename T>
inline double sgn(T arg) {
  if(arg<0) return -1;
  else if(arg>0) return 1;
  else return 0;
}
bool Cylinder::intersect(const Ray &_ray,
                         vec3 &_intersection_point,
                         vec3 &_intersection_normal,
                         double &_intersection_t) const {
  /** \todo
   * - compute the first valid intersection `_ray` with the cylinder
   *   (valid means in front of the viewer: t > 0)
   * - store intersection point in `_intersection_point`
   * - store ray parameter in `_intersection_t`
   * - store normal at _intersection_point in `_intersection_normal`.
   * - return whether there is an intersection with t > 0
  */
  const vec3 &dir = _ray.direction;
  const vec3 normalized_v = normalize(axis);
  const vec3 m = _ray.origin - center;

  double dv = dot(dir, normalized_v);
  double mv = dot(m, normalized_v);


  double a = 1 - dv * dv;
  double b = 2 * (dot(m, dir) - mv * dv);
  double c = dot(m, m) - radius * radius - mv * mv;

  std::array<double, 2> t{};
  size_t nsol = solveQuadratic(a, b, c, t);

  _intersection_t = NO_INTERSECTION;

  // Find the closest valid solution (in front of the viewer)
  for (size_t i = 0; i<nsol; i++) {
    const auto& t_i = t[i];
    vec3 from_center = _ray(t_i) - center;
    double proj_len_axis = abs(dot(from_center, normalized_v));

    if (t_i > 0 && proj_len_axis <= height/2)
      _intersection_t = std::min(_intersection_t, t_i);
  }

  if (_intersection_t == NO_INTERSECTION) return false;

  _intersection_point = _ray(_intersection_t);
  vec3 from_center = _intersection_point-center;
  _intersection_normal = from_center - dot(from_center, normalized_v) * normalized_v;

  _intersection_normal = -sgn(dot(_ray.direction,_intersection_normal))*_intersection_normal;
  _intersection_normal = normalize(_intersection_normal);
  return true;
}

vec3 Cylinder::randomPointOnObject(std::mt19937_64 &g) const {
  std::uniform_real_distribution<double> d(0,1);
  double theta = d(g)*M_2_PI;
  double alongAxis = height*(d(g)-0.5);
  std::array<vec3,3> basis = getBasis(axis);
  vec3 perp = radius*(basis[2]*sin(theta) - basis[3]*cos(theta));
  return perp + axis*alongAxis + center;
}
