//=============================================================================
//
//   Exercise code for the lecture
//   "Introduction to Computer Graphics"
//   by Prof. Dr. Mario Botsch, Bielefeld University
//
//   Copyright (C) Computer Graphics Group, Bielefeld University.
//
//=============================================================================

//== includes =================================================================

#include "StopWatch.h"
#include "Scene.h"
#include "../include/argagg/argagg.hpp"
#include "ImageWindow.h"

#include <vector>
#include <iostream>
#include <string>
#include <fstream>
#include <optional>
#include <X11/Xlib.h>

#include <SFML/Graphics/RenderWindow.hpp>


template<class T>
bool exists(const char *name, const argagg::parser_results &args) {
  try {
    args[name].as<T>();
    return true;
  } catch (...) {
    return false;
  }

}

/// Program entry point.
int main(int argc, char **argv) {
  // Parse input scene file/output path from command line arguments
  struct PathtraceJob {
    std::string scenePath, outPath;
  };

  std::vector<PathtraceJob> jobs;
  bool NEE;
  bool split_view;
  uint samplesPerPixel;
  uint numThreads = 1;

  argagg::parser argparser{{
                                   {"num_samples", {"-n", "--num-samples"}, "Number of samples per pixel (Mandatory)", 1},
                                   {"in_file", {"-i", "--input"}, "Input file (Mandatory)", 1},
                                   {"out_file", {"-o", "--output"}, "Output file (Optional, file name will be generated from input file name if left blank)", 1,},
                                   {"no_nee", {"--no-NEE"}, "Turn on Next Event Estimation"},
//                                   {"all", {"-0"}, "Render all"},
                                   {"num_threads", {"-t", "--num-threads"}, "Number of threads (Optional, defaults to 1)", 1},
                                   {"graphical", {"-g"}, "Enable graphical visualisation"},
                                   {"tile", {"--tile"}, "Tile based animation (Graphical mode needs to be enabled)"},
                                   {"noise", {"--noise"}, "Noise-demonstrating visualisation (Graphical mode needs to be enabled)"},
                                   {"quad_width", {"--qwidth"}, "Quadrant Width - For tile-based rendering (Optional, defaults to 128)", 1},
                                   {"quad_height", {"--qheight"}, "Quadrant Height - For tile-based rendering (Optional, defaults to 128)", 1},
                                   {"noise_step", {"--noise-step"}, "Rate at which different values of SPP are increased (Optional, defaults to 1)", 1},
                                   {
                                           "split_view", {"--split-view"},
                                           "With this setting, half of the screen space will"
                                           " be rendered with Next event estimation, and the other half without"
                                   },
                                   {"autoclose", {"--autoclose"}, "Setting this flag will automatically close the window once the render has finished."}
                           }};
  argagg::parser_results args;
  try {
    args = argparser.parse(argc, argv);
  } catch (const std::exception &e) {
    std::cerr << e.what() << std::endl;
  }

  if (exists<uint>("num_samples", args) && static_cast<bool>(args["in_file"])) {
    split_view = args["split_view"];
    NEE = !args["no_nee"];
    samplesPerPixel = args["num_samples"];
    std::string inFile = args["in_file"];

    std::string outFile;
    if (args["out_file"])
      outFile = args["out_file"].as<std::string>();
    else {
      std::string fileName;
      size_t last_slash = inFile.rfind('/');
      if (last_slash == std::string::npos || last_slash == inFile.length() - 1) {
        fileName = inFile;
      } else {
        size_t last_period = inFile.rfind('.');
        if (last_period != std::string::npos && last_period > last_slash) {
          fileName = inFile.substr(last_slash + 1, last_period - last_slash - 1);
          std::cout << fileName << std::endl;
        } else
          fileName = inFile.substr(last_slash + 1);
      }
      outFile = fileName + "_";
      if (NEE) {
        outFile += "NEE";
      } else {
        outFile += "NONEE";
      }
      outFile += "_" + std::to_string(samplesPerPixel) + ".png";
    }
    std::cout << "Will write to file: " << outFile << std::endl;
    jobs.emplace_back(PathtraceJob{inFile, outFile});
  } else {
    std::cerr << "USAGE: " << argv[0] << " -i <input_file> -o <output_file> -n <num_samples>" << std::endl
//              << "Input and output may be omitted if the -0 option is present" << std::endl
              << argparser << std::endl;
    exit(1);
  }

  if (exists<uint>("num_threads", args)) {
    numThreads = args["num_threads"];
  }

  std::optional window = std::optional<ImageWindow>();

  Scene::VisualisationParam visParam{};
  visParam.split_view = split_view;
  if (args["graphical"]) {
    if (args["tile"]) {
      visParam.kind = Scene::VisualisationType::MBudgetCinebench;

      if (exists<uint>("quad_width", args)) {
        visParam.quadrantSize.x = args["quad_width"];
      } else {
        visParam.quadrantSize.x = 128;
      }

      if (exists<uint>("quad_height", args)) {
        visParam.quadrantSize.y = args["quad_height"];
      } else {
        visParam.quadrantSize.y = 128;
      }
    } else if (args["noise"]) {
      visParam.kind = Scene::VisualisationType::Noise;
      if (exists<uint>("noise_step", args)) {
        visParam.noiseStep = args["noise_step"];
        if(visParam.noiseStep==0) {
          std::cerr << "Noise step cannot be 0!" << std::endl;
          exit(EXIT_FAILURE);
        }
      } else {
        visParam.noiseStep = 1;
      }
    } else {
      visParam.kind = Scene::VisualisationType::NoAnim;
    }

    if (args["noise"] && args["tile"]) {
      std::cerr << "Only one of --tile or --noise can be present at once" << std::endl;
      exit(EXIT_FAILURE);
    }

    // start different thread for a window
    XInitThreads();
    window.emplace();
    window->run();
  }

  //*
  for (const auto &job : jobs) {
    std::cout << "Read scene '" << job.scenePath << "'..." << std::flush;
    Scene s(job.scenePath);
    std::cout << "\ndone (" << s.numObjects() << " objects)\n";

    StopWatch timer;
    std::cout << "Ray tracing..." << std::flush;
    timer.start();
    auto image = s.render(samplesPerPixel, NEE, numThreads, window, visParam, args["autoclose"]);
    timer.stop();
    std::cout << " done (" << timer << ")\n";

    std::cout << "Write image...";
    image.write(job.outPath);
    std::cout << "done\n";
  }//*/

  return 0;
}
