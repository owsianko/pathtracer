//
// Created by sapphie on 24.05.19.
//

#ifndef RAYTRACING_IMAGEWINDOW_H
#define RAYTRACING_IMAGEWINDOW_H


#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Text.hpp>
#include <thread>
#include "vec3.h"

class ImageWindow : public sf::RenderWindow {
public:
  ImageWindow();
  ~ImageWindow() override;
  void run();
  void setSize(uint width, uint height);
  void setQuadrantSize(uint quadWidth, uint quadHeight);
  void setQuadrant(uint x, uint y, const std::vector<vec3> &quadrant);
  void setDrawText(bool b);
  void setNumSamples(uint numSamples);
private:
  void runWindow();
  std::thread execution;
  sf::Image image;
  sf::Texture texture;
  sf::Text text;
  sf::Font font;
  bool drawText = false;
  uint qWidth = 500;
  uint qHeight = 500;
};


#endif //RAYTRACING_IMAGEWINDOW_H
